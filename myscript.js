if (document.title.indexOf("Google") != -1)
{
	//Put in the CSS to style the save button properly. Yes I know this is a hack, but I haven't found
	//another way to do this. I encourage anyone to replace this method with a much better way.
	var css=".saveButtonSB:hover" +
	"{"+
	    "background-image:-webkit-gradient(linear,left top,left bottom,from(#4d90fe),to(#357ae8));" +
	    "background-color:#357ae8;" +
	    "background-image:-webkit-linear-gradient(top,#4d90fe,#357ae8);" +
	    "background-image:linear-gradient(top,#4d90fe,#357ae8);" +
	    "border:1px solid #2f5bb7;" +
	"}" +
	".saveButtonSB" +
	"{"+
		"background-color: #4285F4;" +
		"border: 2px solid #4285F4;" +
		"border-radius: 2px;" +
		"color: #FFFFFF;" +
		"height: 40px;" +
		"width: 60px;" +
		"margin-left: 3px;" +
		"margin-right: 3px;" +
		"font-weight: bold;" + 
		"position: relative;" +
		"left: 37px;" +
		"top: -40px;" + 
	"}";
	var style=document.createElement('style');
	if (style.styleSheet)
	{
	    style.styleSheet.cssText=css;
	}
	else
	{
	    style.appendChild(document.createTextNode(css));
	}
	document.getElementsByTagName('head')[0].appendChild(style);
	
    //Creating Elements
    var btn = document.createElement("button");
	btn.className = "saveButtonSB";
    var t = document.createTextNode("SAVE");
    btn.appendChild(t);
    var searchDiv = document.querySelectorAll("button[type=submit]")[0].parentNode.parentNode;
    if(searchDiv != null)
    {
		//Appending to DOM
		btn.onmousemove = function(event)
		{
			event.stopPropagation();	
		};
		btn.onclick = function(event)
		{
			event.stopPropagation();
		    chrome.storage.sync.get("SearchBuddyInfo", function (obj)
			{
				var info = obj.SearchBuddyInfo;
				var searchTextbox = document.querySelectorAll("input[type=text]")[0];//document.getElementById("gbqfq");
				var searchText = searchTextbox.value;
				var alreadyContains = false;
				if(info != null)
				{
				    alreadyContains = info.filter(function (e)
					{
						return e.search == searchText;
					}).length != 0;
				    if(!alreadyContains)
				    {
						info.push({"search": searchText, "annotation": ""});
				    }
				}
				else
				{
				    info = [{"search": searchText, "annotation": ""}];
				}
				console.log("About to save searchbuddy information!");
				chrome.storage.sync.set({'SearchBuddyInfo': info}, function() {
				    // Notify that we saved.
	                console.log("The value about to be saved is:: " + info[0].search);
				    console.log('Settings saved');		
				});
				var tes = document.getElementById("errorSpanSB");
				if(tes != null)
				{
				    tes.parentNode.removeChild(tes);
				}
				if(alreadyContains)
				{
				    var errorSpan = document.createElement("span");
				    errorSpan.id = "errorSpanSB";
				    errorSpan.style.color = "red";
				    var errorSpanTextNode = document.createTextNode("Duplicate Search");
				    errorSpan.appendChild(errorSpanTextNode);
				    searchDiv.parentNode.appendChild(errorSpan);
				    window.setTimeout(function ()
					{
						var tes = document.getElementById("errorSpanSB");
						tes.parentNode.removeChild(tes);
					}, 5000);
				}
				else
				{
				    chrome.storage.sync.set({'SearchBuddyInfo': info}, function() {
						// Notify that we saved.
						console.log('Settings saved');		
				    });
				}
			});
		};
		searchDiv.appendChild(btn);
   }
}
